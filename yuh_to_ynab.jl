#!/usr/bin/env julia

println("Début script yuh_to_ynab.")

using CSV, DataFrames, Dates, FileIO, PDFIO
import Base.basename

# Vérification du nombre d'arguments
if length(ARGS) != 1
    println("Usage: $(basename(PROGRAM_FILE)) filename")
    exit(1)
end

# Récupération du nom de fichier en argument
input_file = ARGS[1]

# Ouverture du document PDF
doc = pdDocOpen(input_file)

# Récupère le nombre de page
npage = pdDocGetPageCount(doc)

# Crée le buffer
io = IOBuffer(UInt8[], read=true, write=true, append=true)

# Itère sur les différentes pages et alimente le buffer avec le texte du document
for i = 2:npage
    # Récupère le contenu d'une page
    page = pdDocGetPage(doc, i)
    pdPageExtractText(io, page)
end

# Lecture du buffer en lignes
lines = readlines(io)

# Ferme le document PDF
pdDocClose(doc)

println("PDF lu")

# Regex pour la ligne de tableau sur 1 ligne de texte (2 variantes)
regex_1_ligne = r"^\s{2}\d{2}\.\d{2}\.\d{4}(.+)\s?\d{9,}\s{2}(\d*'?\d+\.\d{2})?\s{2}(\d*'?\d+\.\d{2})?\s(\d{2}\.\d{2}\.\d{4})\s{2}(\d*'?\d+\.\d{2})$|^\s{2,}\d{2}\.\d{2}\.\d{4}(.+)\s?\d{9,}\s{3}(\d*'?\d+\.\d{2})?\s{3}(\d*'?\d+\.\d{2})?\s(\d{2}\.\d{2}\.\d{4})\s{2}(\d*'?\d+\.\d{2})$"

# Regex pour la ligne de tableau sur 1 ligne de texte
regex_2_lignes = r"^\s{3,}(.+)\d{2}\.\d{2}\.\d{4}\s{3,}\d{9,}\s+(\d*'?\d+\.\d{2})?\s+(\d*'?\d+\.\d{2})?(\d{2}\.\d{2}\.\d{4})\s{2}(\d*'?\d+\.\d{2})(.*)$"

# Récupération des lignes correspondantes à la 1ère regex
spending_data = filter(contains(regex_1_ligne), lines)

# Récupération des lignes correspondantes à la 2nde regex
spending_data_2 = filter(contains(regex_2_lignes), lines)

# Crée le DataFrame de sortie et le format de date
df = DataFrame(Date=Date[], Payee=String[], Memo=String[], Amount=Float64[])
dateformat = dateformat"dd.mm.yyyy"

# Alimente le DataFrame avec les extractions de la regex de 1 ligne
for line in spending_data
    m = match(regex_1_ligne, line)
    if isnothing(m[10])
        if isnothing(m[3])
            push!(df, (Date(m[4], dateformat), "", strip(m[1]), -parse(Float64, m[2])))
        else
            push!(df, (Date(m[4], dateformat), "", strip(m[1]), parse(Float64, m[3])))
        end
    else
        if isnothing(m[8])
            push!(df, (Date(m[9], dateformat), "", strip(m[6]), -parse(Float64, m[7])))
        else
            push!(df, (Date(m[9], dateformat), "", strip(m[6]), parse(Float64, m[8])))
        end
    end
end

# Alimente le DataFrame avec les extractions de la regex de 1 ligne
for line in spending_data_2
    m = match(regex_2_lignes, line)
    if isnothing(m[3])
        push!(df, (Date(m[4], dateformat), "", strip(m[1]), -parse(Float64, m[2])))
    else
        push!(df, (Date(m[4], dateformat), "", strip(m[1]), parse(Float64, m[3])))
    end
end

println("DataFrame alimenté.")

# Écriture des données dans le fichier de sortie
filename_tuple = splitext(basename(input_file))
output_file = joinpath(dirname(input_file), filename_tuple[1] * "_ynab" * ".csv")
CSV.write(output_file, df)


println("Conversion complète. Fichier: $output_file")
