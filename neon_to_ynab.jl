#!/usr/bin/env julia

println("Début script neon_to_ynab.")

using CSV, DataFrames, Dates, FileIO
import Base.basename

# Vérification du nombre d'arguments
if length(ARGS) != 1
    println("Usage: $(basename(PROGRAM_FILE)) filename")
    exit(1)
end

# Récupération du nom de fichier en argument
input_file = ARGS[1]

# Chargement des données CSV
data = CSV.read(input_file, DataFrame)

println("Fichier $input_file chargé.")

# Suppression des colonnes
select!(data, Not(Symbol.(["Original amount", "Original currency", "Exchange rate", :Category, :Tags, :Wise, :Spaces])))

# Renommage des colonnes
rename!(data, [:Description => :Payee, :Subject => :Memo])

# Réorganisation des colonnes
data = data[:, [:Date, :Payee, :Memo, :Amount]]

println("Colonnes modifiées.")

# Écriture des données dans le fichier de sortie
filename_tuple = splitext(basename(input_file))
output_file = joinpath(dirname(input_file), filename_tuple[1] * "_ynab" * filename_tuple[2])
CSV.write(output_file, data)

println("Conversion complète. Fichier: $output_file")
